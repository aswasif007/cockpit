const fs = require('fs-extra');
const package = require('./package.json');
const readline = require('readline-sync');

function configuration() {
    var name = readline.question(`Application name (${package.name}): `);
    if (name) package.name = name;

    var version = readline.question(`Version (${package.version}): `);
    if (version) package.version = version;

    fs.writeJsonSync(`package.json`, package, {spaces: 4});
}

function electronize() {
    const temp_dir = '.temp';
    fs.removeSync(temp_dir);
    fs.mkdirSync(temp_dir);

    for (var key in package.dependencies) {
        fs.copySync(`node_modules/${key}`, `${temp_dir}/third_party/${key}`)
    }

    fs.copySync(`dist`, `${temp_dir}/dist`);
    fs.copySync(`main.js`, `${temp_dir}/main.js`);
    fs.copySync(`assets`, `${temp_dir}/assets`);

    var html = fs.readFileSync(`index.html`, 'utf-8');
    fs.writeFileSync(`${temp_dir}/index.html`, html.split('node_modules/').join('third_party/'), 'utf-8');

    fs.writeJsonSync(`${temp_dir}/package.json`, {
        "name": package.name,
        "version": package.version,
        "description": package.description,
        "main": "main.js",
        "scripts": {
            "start": "electron ."
        },
        "author": package.author,
        "license": package.license
    });

    console.log(`\x1b[32mBuild files are stored at ${temp_dir} directory.\x1b[0m`);
}

if (process.argv.length < 3) {
    configuration();
} else if (process.argv[2] == 'build') {
    electronize();
} else if (process.argv[2] == '-d') {
    fs.ensureDirSync('dist');
} else if (process.argv[2] == 'clean') {
    fs.removeSync('.temp');
    fs.removeSync('deploy');
    fs.removeSync('dist');
}
