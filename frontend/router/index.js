import Vue from 'vue'
import Router from 'vue-router'
import Dashboard from '../components/Dashboard.vue'
import Repositories from '../components/Repositories.vue'

Vue.use(Router)

export default new Router({
    routes: [{
        path: '/dashboard',
        name: 'Dashboard',
        component: Dashboard
    }, {
        path: '/repositories',
        name: 'Repositories',
        component: Repositories
    }]
})
