# Cockpit

> A desktop application for maintaining developer's headaches.

## Build Setup

``` bash
# install dependencies
npm install && pip install -r backend/requirements.txt

# serve with hot reload at localhost:8080
npm start

# build for production with minification
npm run build

# deploy as native app
npm run deploy

# run configuration
npm run config

# clean temporary files
npm run clean
```

## Version
> 0.1.0
