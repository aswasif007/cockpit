import git, os, subprocess, sys

def get_repositories(workspace):
    folders = [folder for folder in os.listdir(workspace) if os.path.isdir(os.path.join(workspace, folder))]
    
    repositories = []

    for folder in folders:
        try:
            repo = git.Repo(os.path.join(workspace, folder))
            
            repo_info = {
                'name': folder,
                'directory': repo.working_dir,
                'is_dirty': repo.is_dirty(),
                'branches': [branch.name for branch in repo.heads],
                'active_branch': repo.active_branch.name,
                'remotes': [{'name': remote.name, 'url': remote.url} for remote in repo.remotes]
            }
            
            repositories.append(repo_info)
        except:
            continue

    return repositories
###

def switch_branch(directory, branch):
    repo = git.Repo(directory)

    try:
        repo.git.checkout(branch)
        return {'status': 200, 'msg': ''}
    except Exception as e:
        return {'status': 400, 'msg': e.stderr}
###

def open_in_editor(directory, editor):
    _map = {'darwin': "open -a '{}'".format(editor), 'linux2': "'{}'".format(editor), 'win32': "'{}'".format(editor)}

    proc = subprocess.Popen("{} {}".format(_map[sys.platform], directory), shell=True, stderr=subprocess.PIPE)
    stderr = proc.stderr.readlines()

    if len(stderr) > 0:
        err = ''.join(stderr)
        return {'status': 400, 'msg': err}
    else:
        return {'status': 200, 'msg': ''}
###

def open_in_folder(directory):
    _map = {'darwin': 'open', 'win32': 'explorer', 'linux2': 'xdg-open'}
    os.system('{} {}'.format(_map[sys.platform], directory))
    return {'status': 200, 'msg': ''}
###
