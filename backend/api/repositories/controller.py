from flask import Blueprint, jsonify, request
from api.repositories import service
from api.settings import service as settings_service

import json

repositories = Blueprint(__name__, 'repositories')

@repositories.route('/')
def index():
    workspaces = settings_service.get_settings()['workspaces']

    repositories = []
    for workspace in workspaces:
        repositories += service.get_repositories(workspace)

    res = {
        'workspaces': workspaces,
        'repositories': repositories,
    }
    
    return jsonify(res)
###

@repositories.route('/switch_branch', methods=['POST'])
def switch_branch():
    data = json.loads(request.data)
    res = service.switch_branch(data['directory'], data['branch'])

    return jsonify(res)
###

@repositories.route('/open_in_editor', methods=['POST'])
def open_in_editor():
    data = json.loads(request.data)
    res = service.open_in_editor(data['directory'], editor=settings_service.get_settings()['editor'])

    return jsonify(res)
###

@repositories.route('/open_in_folder', methods=['POST'])
def open_in_folder():
    data = json.loads(request.data)
    res = service.open_in_folder(data['directory'])

    return jsonify(res)
###
