import os

HOST = 'localhost'
PORT = 13131
LOCAL = '{}/Documents/Cockpit'.format(os.environ['HOME'])
SETTINGS = '{}/settings.json'.format(LOCAL)
