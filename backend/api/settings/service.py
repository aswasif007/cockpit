import os, json
from api.utils import LOCAL, SETTINGS

def get_settings():
    if not os.path.exists(LOCAL):
        os.makedirs(LOCAL)
    
    if os.path.exists(SETTINGS):
        return json.load(open(SETTINGS))
    else:
        ob = {
            'workspaces': [],
            'editor': '',
        }
        json.dump(ob, open(SETTINGS, 'w'))
        return ob
###

def update_settings(ob):
    settings = get_settings()
    settings.update(ob)
    json.dump(settings, open(SETTINGS, 'w'))
    return settings
###
