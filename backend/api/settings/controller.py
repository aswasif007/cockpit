import json

from flask import Blueprint, jsonify, request
from api.settings import service

settings = Blueprint(__name__, 'settings')

@settings.route('/')
def index():
    res = service.get_settings()
    return jsonify(res)
###

@settings.route('/', methods=['POST'])
def update_settings():
    data = json.loads(request.data)
    res = service.update_settings(data)
    print res
    return jsonify(res)
###
