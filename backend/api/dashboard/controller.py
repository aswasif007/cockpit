from flask import Flask, Blueprint, jsonify

dashboard = Blueprint(__name__, 'dashboard')

@dashboard.route('/')
def index():
    res = {
        'msg': 'hello from the other side!'
    }
    
    return jsonify(res)
###
