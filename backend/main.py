from flask import Flask, jsonify, request
from api.dashboard.controller import dashboard
from api.repositories.controller import repositories
from api.settings.controller import settings
from api.utils import HOST, PORT
from flask_cors import CORS

app = Flask(__name__)
app.register_blueprint(dashboard, url_prefix='/dashboard')
app.register_blueprint(repositories, url_prefix='/repositories')
app.register_blueprint(settings, url_prefix='/settings')

@app.errorhandler(404)
def anypath(path):
    res = {
        'msg': 'Oops! Looks like you did a boo boo!'
    }

    return jsonify(res)
###

@app.after_request
def after_request(response):
    return response
###

if __name__ == '__main__':
    CORS(app)
    app.run(host=HOST, port=PORT)